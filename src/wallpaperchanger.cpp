#include "wallpaperchanger.h"
#include "filedownloader.h"

QUrl wallpaperchanger::getWallpaperUrl() {
    QStringList args = QCoreApplication::arguments();
    QUrl wallpaperUrl;

    QString firstArg = "wallpaper";
    if (args.size() > 1) {
        firstArg = args[1];
    }

    if (firstArg.startsWith("http")) {
        wallpaperUrl = firstArg;
    } else {
        QString displayResolution = getMaxDisplayResolution();
        QString urlString = "https://source.unsplash.com/" + displayResolution + "/?" + firstArg;
        wallpaperUrl = QUrl(urlString);
    }

    qDebug() << wallpaperUrl;

    return wallpaperUrl;
}

QString wallpaperchanger::getMaxDisplayResolution() {
    QString reso = executeCommand("xrandr | grep \\* | cut -d' ' -f4");

    if (reso.contains("\n")) {
        const QStringList &resolutions = reso.trimmed().split("\n");
        reso = resolutions[0];
        int resoInt = toResoInt(reso);
        for (const auto &resolution : resolutions) {
            int iInt = toResoInt(resolution);
            if (iInt > resoInt) {
                reso = resolution;
            }
        }
    }

    return reso.trimmed();
}

QString wallpaperchanger::executeCommand(const QString &command) {
    QProcess process;
    process.start("bash", QStringList() << "-c" << command);
    process.waitForFinished(-1); // will wait forever until finished

    QString stdout = process.readAllStandardOutput();
    QString stderr = process.readAllStandardError();
//    qDebug() << stderr;
    return stdout;
}

void wallpaperchanger::setWallpaper(const QString &fileName) {
    QString desktop = detectDesktop();

    if (desktop.contains("gnome")) {
        executeCommand("gsettings set org.gnome.desktop.background picture-uri file://" + fileName);
        executeCommand("gsettings set org.gnome.desktop.background picture-options scaled");
        executeCommand("gsettings set org.gnome.desktop.screensaver picture-uri file://" + fileName);
        executeCommand("gsettings set org.gnome.desktop.screensaver picture-options scaled");
    } else if (desktop.contains("cinnamon")) {
        executeCommand("gsettings set org.cinnamon.desktop.background picture-uri file://" + fileName);
        executeCommand("gsettings set org.cinnamon.desktop.background picture-options scaled");
        executeCommand("gsettings set org.cinnamon.desktop.screensaver picture-uri file://" + fileName);
        executeCommand("gsettings set org.cinnamon.desktop.screensaver picture-options scaled");
    } else if (desktop.contains("deepin")) {
        executeCommand("gsettings set com.deepin.wrap.gnome.desktop.background picture-uri file://" + fileName);
        executeCommand("gsettings set com.deepin.wrap.gnome.desktop.background picture-options scaled");
        executeCommand("gsettings set com.deepin.wrap.gnome.desktop.screensaver picture-uri file://" + fileName);
        executeCommand("gsettings set com.deepin.wrap.gnome.desktop.screensaver picture-options scaled");
    } else if (desktop.contains("plasma") || desktop.contains("kde")) {
        executeCommand(createKdePlasmaCommand(fileName));
    } else if (desktop.contains("xfce")) {
        if (QFile::exists("/bin/feh") || QFile::exists("usr//bin/feh")) {
            executeCommand("feh --bg-scale " + fileName);
        } else {
            executeXfceCommand(fileName);
        }
    } else {
        qInfo() << desktop << " is not supported yet.";
    }

    emit wallpaperChanged();
}

QString wallpaperchanger::detectDesktop() {
    return QString(std::getenv("XDG_CURRENT_DESKTOP")).toLower();
}

QString wallpaperchanger::createKdePlasmaCommand(const QString &fileName) {
    QString kdeCommand = QString(
            "dbus-send --session --dest=org.kde.plasmashell --type=method_call /PlasmaShell org.kde.PlasmaShell.evaluateScript 'string:"
            "var Desktops = desktops();"
            "for (i=0;i<Desktops.length;i++) {"
            "        d = Desktops[i];"
            "        d.wallpaperPlugin = \"org.kde.image\";"
            "        d.currentConfigGroup = Array(\"Wallpaper\","
            "                                    \"org.kde.image\","
            "                                    \"General\");"
            "        d.writeConfig(\"Image\", \"file://" + fileName + "\");"
                                                                      "}'");

    return kdeCommand;
}

void wallpaperchanger::executeXfceCommand(const QString &fileName) {
    const QString &channels = executeCommand("xfconf-query -c xfce4-desktop -l | grep \"last-image$\"");
    const QStringList &channelList = channels.split("\n");

    for (const QString &channel : channelList) {
        executeCommand("xfconf-query --channel xfce4-desktop --property " + channel + " --set " + fileName);
    }
}

int wallpaperchanger::toResoInt(QString resoString) {
    const QStringList &list = resoString.split("x");
    int a = list[0].toInt();
    int b = list[1].toInt();
    return a + b;
}
