#include <QtCore>

class wallpaperchanger : public QObject {
Q_OBJECT

public:
    QUrl getWallpaperUrl();

signals:

    void wallpaperChanged();

public slots:

    void setWallpaper(const QString& fileName);

private:
    QString getMaxDisplayResolution();

    QString executeCommand(const QString &command);

    QString detectDesktop();

    QString createKdePlasmaCommand(const QString &fileName);

    void executeXfceCommand(const QString &fileName);

    int toResoInt(QString resoString);
};
