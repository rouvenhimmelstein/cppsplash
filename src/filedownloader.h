#include <QtNetwork>
#include <QtCore>

class FileDownloader : public QObject {
Q_OBJECT

public:
    void download(QUrl &url, QString &targetFile);

signals:

    void downloaded(QString string);

private slots:

    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);

    void downloadFinished();

    void downloadReadyRead();

private:
    QNetworkReply *networkReply = nullptr;
    QFile output;
    QTime downloadTime;
};