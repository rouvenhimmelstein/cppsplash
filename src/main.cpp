#include "filedownloader.h"
#include "wallpaperchanger.h"

QString getHomeWallpaperFile();

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg) {
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
        case QtDebugMsg:
#ifdef NDEBUG
#else
            fprintf(stderr, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line,
                    context.function);
#endif
            break;
        case QtInfoMsg:
            fprintf(stderr, "Info: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line,
                    context.function);
            break;
        case QtWarningMsg:
            fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line,
                    context.function);
            break;
        case QtCriticalMsg:
            fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line,
                    context.function);
            break;
        case QtFatalMsg:
            fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line,
                    context.function);
            abort();
    }
}

int main(int argc, char *argv[]) {
    qInstallMessageHandler(myMessageOutput);

    QCoreApplication app(argc, argv);

    QString file = getHomeWallpaperFile();

    wallpaperchanger wallpaperChanger;
    FileDownloader fileDownloader;

    QObject::connect(&fileDownloader, SIGNAL(downloaded(QString)), &wallpaperChanger, SLOT(setWallpaper(QString)));
    QObject::connect(&wallpaperChanger, SIGNAL(wallpaperChanged()), &app, SLOT(quit()));

    QUrl wallpaperUrl = wallpaperChanger.getWallpaperUrl();
    fileDownloader.download(wallpaperUrl, file);

    return QCoreApplication::exec();
}

QString getHomeWallpaperFile() {
    QDir homeDir = QDir::home();
    homeDir.mkdir(".wallpaper");
    homeDir.cd(".wallpaper");
    return homeDir.absoluteFilePath("wallpaper.jpeg");
}