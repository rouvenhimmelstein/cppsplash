# cppSplash

chwp provides the ability to change the wallpaper from the command line.

## SYNOPSIS
       chwp [keywords]|[imageurl]

## OPTIONS
       span  -  spans a picture over all monitors

## EXAMPLES
       chwp - Changes the wallpaper and lockscreen wallpaper randomly

       chwp water,nature - Changes the wallpaper according to the given keywords

       chwp https://source.unsplash.com/daily - Changes the wallpaper by a given image url

       chwp water,nature /home/user/Pictures https://source.unsplash.com/daily - Define multiple wallpaper sources, a random one is picked

## Wallpaper rotation

Can be done via crontab.

## AUTHOR

Rouven Himmelstein (rouvenhimmelstein@gmail.com)
